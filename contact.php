<?php 
$thankYou = $error = "";
if(isset($_POST['submit'])){
	$name = $_POST['name'];
	//the address the email will be sent to
	$receipientEmail = "paymintcrew@gmail.com";
	//contact persons email
	$senderEmail = trim($_POST['email']);
	//email message
	$message = $_POST['message'];
	//email subject
	$subject = trim($_POST['subject']);
	//body of the email
	$mailBody = "Name: $name\nEmail: $senderEmail\n\n$message";
	
    //sends an email to the recepient email with the mail body and subject, returns true if the email was sent and false otherwise
	if(mail($receipientEmail, $subject, $mailBody, "From: <$senderEmail>")){
		$thankYou = "Thank You your Message has been sent";
	}else{
		$error = "Error sending your message, please try again";
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/couchgame.css">
	<title>Couch Game</title>
	<style>
		body{
			background: url('images/background/rsz_bridgeunderpass.jpg') no-repeat fixed center center;
			background-size: cover;
			
		}	
	</style>
</head>
<body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	<script src="http://cdn.jsdelivr.net/jquery.validation/1.14.0/jquery.validate.min.js"></script>
	<script src="js/bootstrap.js"></script>
	<div class="navbar navbar-inverse">
		<div class="container">
			<a href="index.html" class="navbar-brand"><img src="images/cg.png" height="100"></a>
			<button class="navbar-toggle" data-toggle="collapse" data-target=".navHeaderCollapse">
				<span class="icon-bar"</span>
			</button>
			<div class="collapse navbar-collapse navHeaderCollapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="index.html">HOME</a></li>
					<li><a href="trailers.html">TRAILERS</a></li>
					<li><a href="art.html">ART</a></li>
					<li><a href="team.html">MEET THE TEAM</a></li>
					<li class="active"><a>CONTACT</a></li>
				</ul>
			</div>
		</div>
	</diV>
	<div class="container top">
		<div class="well well-sm">
			<h2>Contact Us</h2>
			<div class="success"><?php echo $thankYou;//echo success message to the user notifying them of successful email sent?></div>
			<div class="message-error"><?php echo $error//echos out that there was a problem sending the email?></div>
			<form action="contact.php" method="post" id="contact-form">
				<div class="form-group">
					<label for="Name">Name(First Last):</label>
					<input type="text" name="name" class="form-control" id="Name" placeholder="John Doe">
				</div>
				<div class="form-group">
					<label for="EmailAddress">Email Address:</label>
					<input type="email" name="email" class="form-control" id="EmailAddress" placeholder="example@example.com">
				</div>
				<div class="form-group">
					<label for="subject">Subject</label>
					<input type="text" name="subject" class="form-control" id="subject">
				</div>
				<div class="form-group">
					<label>Message:</label>
					<textarea rows="5" cols="30" name="message"></textarea>
				</div>
				<input type="submit" name="submit" class="btn btn-primary">			
			</form>
			<div>
				<p>or by clicking <a href="mailto:paymintcrew@gmail.com">here</a> you can email us directly using your email client</p>
			</div>
		</div>
	</div>
</body>
	<footer class="nav navbar-inverse navbar-fixed-bottom">
		<div class="container">
			<p class="copyright">&copy; 2015-<span id="year"></span> <a href="#">Paymint Crew</a></p>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="https://www.facebook.com/couchgame"><img src="images/fb-1.png" alt="Facebook logo" class="footer-social"></a></li>
					<li><a href="https://www.youtube.com/channel/UC-VutZ30wBBlJ8-V1yOUE9A"><img src="images/yt.png" alt="Youtube Logo" class="footer-social"></a></li>
					<li><a href="https://twitter.com/couch_game"><img src="images/twitter.png" alt="Twitter Logo" class="footer-social"></a></li>
					<li><a href="http://www.twitch.tv/paymintcrew"><img src="images/twitchlogo.png" alt="Twitch Logo" class="footer-social"></a></li>
				</ul>
		</div>
		<script> document.getElementById('year').innerHTML = new Date().getFullYear();</script>
	</footer>
<script>
	$(document).ready(function(){
		$("#contact-form").validate({
			rules:{
				name: {
					required: true,
					minlength: 5
				},
				email:{
					required: true,
					email: true
				},
				message:{
					required: true
				},
				subject: {
					required: true,
					minLength: 4
				}
			}
		});	
	});
</script>
</html>
	